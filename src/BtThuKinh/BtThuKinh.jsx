import React, { Component } from "react";
import { dataGlasse } from "./DataGlasses";

export default class BtThuKinh extends Component {
  state = {
    glassArr: dataGlasse,
    glass: dataGlasse[0],
  };
  renderList = () => {
    return this.state.glassArr.map((item, index) => {
      console.log("item: ", item);
      return (
        <img
          key={index}
          onClick={() => {
            this.handleChangeGlass(item);
          }}
          style={{
            width: `${100 / this.state.glassArr.length}%`,
            padding: "0px 15px",
          }}
          src={item.url}
          alt=""
        />
      );
    });
  };
  handleChangeGlass = (item) => {
    this.setState({
      glass: item,
    });
  };

  render() {
    return (
      <div className="bg-image vh-100">
        <div className="p-5 bg-title">
          <h2>TRY GLASSES APP ONLINE</h2>
        </div>
        <div className="model bg-center bg-cover h-96 w-80 mt-6">
          <img
            className="w-20 absolute top-24 left-20 opacity-80"
            src={this.state.glass.url}
          />
        </div>
        {this.renderList()}
      </div>
    );
  }
}
